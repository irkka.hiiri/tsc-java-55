package ru.tsc.ichaplygina.taskmanager.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class FileScanner implements Runnable {

    @Nullable
    private List<String> commands;

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private List<String> getCommands() {
        return commandService.getArguments().values()
                .stream()
                .map(AbstractCommand::getCommand)
                .collect(Collectors.toList());
    }

    public void init() {
        commands = getCommands();
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        if (commands == null) return;
        @NotNull final String path = propertyService.getFileScannerPath();
        @NotNull final List<String> files = Files.list(Paths.get(path))
                .filter(file -> !Files.isDirectory(file))
                .map(Path::getFileName)
                .map(Path::toString)
                .collect(Collectors.toList());
        for (final String fileName : files) {
            if (commands.contains(fileName)) {
                bootstrap.executeCommand(fileName);
                System.out.println(fileName);
                Files.delete(Paths.get(fileName));
            }
        }
    }

    public void start() {
        executorService.scheduleWithFixedDelay(this, 0, propertyService.getFileScannerFrequency(), TimeUnit.MILLISECONDS);
    }
}
