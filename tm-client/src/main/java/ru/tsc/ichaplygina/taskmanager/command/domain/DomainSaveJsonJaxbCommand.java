package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class DomainSaveJsonJaxbCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save json jaxb";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to json file using jaxb";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().saveJsonJaxb(sessionService.getSession());
    }

}
