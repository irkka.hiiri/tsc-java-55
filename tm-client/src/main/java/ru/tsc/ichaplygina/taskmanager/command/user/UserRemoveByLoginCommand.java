package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "remove user by login";

    @NotNull
    public static final String DESCRIPTION = "remove user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        getAdminEndpoint().removeUserByLogin(sessionService.getSession(), login);
    }

}
