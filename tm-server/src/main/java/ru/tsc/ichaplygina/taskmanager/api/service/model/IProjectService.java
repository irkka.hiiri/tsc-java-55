package ru.tsc.ichaplygina.taskmanager.api.service.model;

import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface IProjectService extends IAbstractBusinessEntityService<Project> {

}
